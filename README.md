# Country Information

Here is a small assignment in React. The purpose is as following: 
> Create a simple web interface using React which allows users to: 

a) Search for countries and display its full country name, capital, population and currency. This can be a simple list, but you can also get creative in how to visualize these data points.

b) Enter an amount in SEK and get the amount converted into local currency for each country. 
 
The data can amongst others be retrieved from the following open APIs: 
  - https://restcountries.eu for country information 
  - http://fixer.io for exchange rates 

## How to run the project
Simply run `yarn` + `yarn start` (OR `npm install` + `npm run start`). 

In order to have the ability to use the currency converter, you need to add a API KEY inside the `.env` file. 
Add you fixer.io API key inside `REACT_APP_FIXER_API_KEY`

## More explanation about the choices

I've runned with what was recommanded: https://restcountries.eu to get the countries and information about them. 

http://fixer.io has been used to get information about currencies. Even though they recently changed the way you can fetch currencies (EUR is the only base you can use) it was pretty easy to calculate an exchange rate. 

I've added a (small) difference from mobile to desktop layout. Not crazy but just a way to see data in a better way depending on the layout of the device. 

## What if I'd have more time ? 

**Problem** First thing first, API call from the frontend.. It will be pretty easy to get the API key by looking at the request in the network tab of your browser. 

**Solution** Using a simple lambda function (Netlify function would've probably worked pretty well for this) we could've moved this api call in the 'backend', and therefore, the API key would've reside there. 

**Problem** The code is very poor in documentation. 

**Solution** Even though I think functions are very straight forwards, maybe some comments here and there wouldn't be too much. 

**Problem** Everything around testing is very poor

**Solution** Even though I've added some tests. Those are very minimal and poor. I think if there is an area where I can improve and learn a lot, it is definitely in testing.

**Problem** Fun facts are actually not that fun

**Solution** I've added some fun facts when you select a country. Those are just information grabbed from the restcountries API. Maybe finding some fun stories and mapping them to the specific countries would've been funnier 😄