import React from 'react';
import { shallow, mount } from 'enzyme';
import App from './App';
import FormPane from './FormPane/FormPane';

it('renders without crashing', () => {
  shallow(<App />);
});

it('App State exists', () => {
  const wrapper = shallow(<App />);
  expect('showInformations' in wrapper.state()).toEqual(true)
  expect('countrySelected' in wrapper.state()).toEqual(true)
});

it('App has default state', () => {
  const wrapper = shallow(<App />);
  expect(wrapper.state('showInformations')).toEqual(false)
  expect(wrapper.state('countrySelected')).toEqual(new Object)
});




