import React, { Component } from 'react';
import CurrencyConverter from '../CurrencyConverter/CurrencyConverter'
import FadeIn from 'react-fade-in';

import './InformationPane.css'

class InformationPane extends Component {

  generateFunFact = () => {
    // Return a random fun fact from an array
    const funFactList = [
      `${this.props.selectedCountry.name} speaks ${this.props.selectedCountry.languages.length} ${this.props.selectedCountry.languages.length <= 1 ? 'language only' : 'different languages'}`,
      `${this.props.selectedCountry.name} can also be spelled ${this.props.selectedCountry.altSpellings[Math.floor(Math.random() * this.props.selectedCountry.altSpellings.length)]}`,
      `${this.props.selectedCountry.name} has ${this.props.selectedCountry.borders.length} land border(s)`,
      `${this.props.selectedCountry.name} is located in ${this.props.selectedCountry.region}`,
      `${this.props.selectedCountry.name} native name is ${this.props.selectedCountry.nativeName}`,
      `${this.props.selectedCountry.name} in French is called ${this.props.selectedCountry.translations.fr}`
    ]
    return (
      <h2 className="informationPaneSubtitle"> Fun fact: did you know that {funFactList[Math.floor(Math.random() * funFactList.length)]}<span aria-label="thumbs up" role="img"> 😲 </span> </h2>
    );
  }

  render() {
    return (
      <div className="informationPane"> 
        {this.props.showInformations ? (
          <FadeIn>
           
            <h1 className="informationPaneTitle">  <img style={{paddingRight : '10px'}} className="flag-icon" alt="country-flag" src={this.props.selectedCountry.flag}/> {this.props.selectedCountry.name} </h1> 
            {this.generateFunFact()}

            <p style={{textDecoration: 'underline'}}> More information: </p>
              <ul> 
                <li> <strong>{this.props.selectedCountry.population.toLocaleString()}</strong> people are living there </li>
                <li> The capital is <strong>{this.props.selectedCountry.capital}</strong> </li>
                <li> The main currency is <strong>{this.props.selectedCountry.currencies[0].name}</strong> </li>
              </ul>
            <hr style={{border: '1px solid white', margin: '40px 80px'}}/>
            <CurrencyConverter 
              countryCurrency={this.props.selectedCountry.currencies[0]}  />
          </FadeIn>
        ): ''}

      </div>
    );
  }
}

export default InformationPane;