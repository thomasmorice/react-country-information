import React, { Component } from 'react';
import './App.css';
import FormPane from './FormPane/FormPane'
import InformationPane from './InformationPane/InformationPane'

class App extends Component {

  state = {
    showInformations: false,
    countrySelected: {}
  }

  onCountryChanged = (countrySelected) => {
    if (!countrySelected) {
      this.setState({ showInformations: false })
    }
    else {
      this.setState({ showInformations: true, countrySelected: countrySelected })
    }
    
  }
  render () {
    return (
      <div className="App">
        <FormPane onCountryChanged={this.onCountryChanged} showInformations={this.state.showInformations}/>
        <InformationPane showInformations={this.state.showInformations} selectedCountry={this.state.countrySelected}/>
      </div>
    );
  }
}

export default App;
