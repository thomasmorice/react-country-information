import React, { Component } from 'react';
import Select from 'react-select';
import 'react-select/dist/react-select.css';

class FormPane extends Component {

  componentDidMount() {
    fetch('https://restcountries.eu/rest/v2/all')
    .then(res => res.json())
    .then((data) => {
      this.setState({ countries: data })
    })
    .catch(console.log)
  }

  state = {
    selectedCountry: '',
    countries: [],
  }
  handleChange = (selectedCountry) => {
    this.setState({ selectedCountry });
    // selectedCountry can be null when the `x` (close) button is clicked
    if (selectedCountry) {
      this.props.onCountryChanged(this.state.countries[selectedCountry.value]);
    } else {
      this.props.onCountryChanged();
    }
  }
  render() {
    const { selectedCountry } = this.state;

    const countryNames = this.state.countries.map(function(country, index) {
      return {
        value: index,
        label: country.name
      }
    });
    return (
      <div className="formPane">
        <h1 className="formPaneTitle"> Search a country </h1> 
        <h2 className="formPaneSubtitle"> Using this smart select input </h2>
        <Select
          className="countrySelector"
          value={selectedCountry}
          onChange={this.handleChange}
          options={countryNames}
        />
      </div>
    );
  }
}

export default FormPane;