import React from 'react';
import { shallow } from 'enzyme';
import FormPane from './FormPane';


it('renders form pane title', () => {
  const wrapper = shallow(<FormPane />);
  const searchTitle = <h1 className="formPaneTitle"> Search a country </h1> ;
  expect(wrapper.contains(searchTitle)).toEqual(true);
});
/*
it('renders form pane search box', () => {
  const wrapper = shallow(<FormPane />);
  console.log(wrapper.debug())
  
  //expect(wrapper.find(formPaneTitle)).to.have.length(1);
});
*/

