import React, { PureComponent } from 'react';
import CurrencyInput from 'react-currency-input';

import './CurrencyConverter.css'

class CurrencyConverter extends PureComponent {
  componentDidMount() {
    this.fetchCurrentCurrency()
  }
  componentDidUpdate() {
    this.fetchCurrentCurrency()
  }
  state = {
    exchangeRate: 0,
    amountInSEK: 1
  }

  fetchCurrentCurrency() {
    fetch(`http://data.fixer.io/api/latest?access_key=${process.env.REACT_APP_FIXER_API_KEY}&symbols=${this.props.countryCurrency.code},SEK`)
    .then(res => res.json())
    .then((data) => {
      this.setState({ exchangeRate: data.rates[this.props.countryCurrency.code] / data.rates.SEK 
      })
    })
    .catch(console.log);
  }

  handleChange(event, maskedValue, floatvalue){
    this.setState({ amountInSEK: floatvalue});
  }

  getAmountInSelectedCurrency() {    
    return Math.round(this.state.exchangeRate * this.state.amountInSEK * 100) / 100
  }

  render() {
    if (process.env.REACT_APP_FIXER_API_KEY) {
      return (
        <div className="currencyConverter" style={{paddingBottom: '20px'}}>
          <h3 className="currencyConverterTitle"> How much a Swedish Krona is worth in {this.props.countryCurrency.name} ? </h3>
          <CurrencyInput className="currencyInput" value={this.state.amountInSEK} onChangeEvent={this.handleChange.bind(this)}/> 
          <span> SEK is <strong> {this.getAmountInSelectedCurrency()}</strong> {this.props.countryCurrency.name}  </span>
        </div>
      );
    } else {
      return (
        <h3> No API key set in `.env` (REACT_APP_FIXER_API_KEY) </h3>
      )
    }

  }
}

export default CurrencyConverter;